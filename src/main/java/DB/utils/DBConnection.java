package DB.utils;

import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public final class DBConnection {
    private static final Logger logger = Logger.getLogger(DBConnection.class);

    private DBConnection() {

    }

    public static Connection getConnection() {
        InitialContext initialContext;
        DataSource dataSource;
        Connection connection = null;
        try {
            initialContext = new InitialContext();
            dataSource = (DataSource) initialContext.lookup("java:comp/env/jdbc/postgres");
            connection = dataSource.getConnection();
        } catch (SQLException | NamingException e) {
            logger.error(e);
        }
        return connection;
    }

}
