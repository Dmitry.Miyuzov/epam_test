package DB;

import DAO.DAOFactory;
import DAO.MessageDAO;
import DAO.TokenDAO;
import DAO.UserDAO;

public class PostgresDAOFactory extends DAOFactory {

    public MessageDAO getMessageDAO() {
        return new PostgresMessageDAO();
    }

    public UserDAO getUserDAO() {
        return new PostgresUserDAO();
    }

    public TokenDAO getTokenDAO() {
        return new PostgresTokenDAO();
    }
}
