package DB;

import DAO.UserDAO;
import DTO.Token;
import DTO.User;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostgresUserDAO implements UserDAO {
    private static final Logger LOGGER = Logger.getLogger(PostgresUserDAO.class);
    private static final String EMPTY = "";
    private static final int ZERO = 0;

    @Override
    public List<User> getAllOnlineUsers() {
        List<User> result = new ArrayList<>();
        try {
            result = PostgresSQLRequestHandler.getAllOnlineUsers();
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public void updatePassword(int id, String pass) {
        try {
            PostgresSQLRequestHandler.updatePassword(id, pass);
            LOGGER.info("The user with id: " + id + " updated password");
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public String getPassword(String email) {
        String result = EMPTY;
        try {
            result = PostgresSQLRequestHandler.getPassword(email);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public String getEmail(String email) {
        String result = EMPTY;
        try {
            result = PostgresSQLRequestHandler.getEmail(email);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public void addUserToDataBase(User user) {
        try {
            PostgresSQLRequestHandler.addUserToDataBase(user);
            LOGGER.info("The user with email: " + user.getEmail() + " completed registration");
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public int getIdUser(String email) {
        int result = ZERO;
        try {
            result = PostgresSQLRequestHandler.getIdUser(email);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public void addTokenToDataBase(Token token) {
        try {
            PostgresSQLRequestHandler.addTokenToDataBase(token);
            LOGGER.info("Token for user with id: " + token.getUserId() + " was added");
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public int getRole(String role) {
        int result = ZERO;
        try {
            result = PostgresSQLRequestHandler.getIdRole(role);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public User getUser(int id) {
        User user = null;
        try {
            user = PostgresSQLRequestHandler.getUser(id);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return user;
    }
}
