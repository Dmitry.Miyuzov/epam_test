package utils;

import DAO.MessageDAO;
import DAO.TokenDAO;
import DAO.UserDAO;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public final class ServletUtils {

    private static final String SERVLET_ATTRIBUTE_MESSAGE_DAO = "messageDao";
    private static final String SERVLET_ATTRIBUTE_USER_DAO = "userDao";
    private static final String SERVLET_ATTRIBUTE_TOKEN_DAO = "tokenDao";

    private ServletUtils() {

    }

    public static UserDAO getUserDao(ServletRequest request) {
        HttpServletRequest req = (HttpServletRequest) request;
        return (UserDAO) req.getServletContext().getAttribute(SERVLET_ATTRIBUTE_USER_DAO);
    }

    public static MessageDAO getMessageDao(ServletRequest request) {
        HttpServletRequest req = (HttpServletRequest) request;
        return (MessageDAO) req.getServletContext().getAttribute(SERVLET_ATTRIBUTE_MESSAGE_DAO);
    }

    public static TokenDAO getTokenDao (ServletRequest request) {
        HttpServletRequest req = (HttpServletRequest) request;
        return (TokenDAO) req.getServletContext().getAttribute(SERVLET_ATTRIBUTE_TOKEN_DAO);
    }
}
