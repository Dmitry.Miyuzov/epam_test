package utils;

import serviсes.*;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class AllocatorRequests {
    private static final String PATTERN_SERVICE_USER = RegexProvider.getServiceExpression("service.users");
    private static final String PATTERN_SERVICE_MESSAGE = RegexProvider.getServiceExpression("service.messages");
    private static final String PATTERN_SERVICE_LOGIN = RegexProvider.getServiceExpression("service.login");
    private static final String PATTERN_SERVICE_REGISTRATION = RegexProvider.getServiceExpression("service.registration");
    private static final String PATTERN_SERVICE_AUTHENTICATION = RegexProvider.getServiceExpression("service.authentication");
    private static final String PATTERN_SERVICE_LOGOUT = RegexProvider.getServiceExpression("service.logout");
    private static final String SERVICE_USER = "users";
    private static final String SERVICE_MESSAGE = "messages";
    private static final String SERVICE_LOGIN = "login";
    private static final String SERVICE_REGISTRATION = "registration";
    private static final String SERVICE_AUTHENTICATION = "authentication";
    private static final String SERVICE_LOGOUT = "logout";
    private static final String EMPTY = "";
    private static final HashMap<String, String> PATTERN_SERVICE_MAP = new HashMap<>();

    private AllocatorRequests() {

    }

    public static Service getService(String uri) {
        Service service;
        String nameService = getNameService(uri);

        switch (nameService) {
            case SERVICE_USER:
                service = new UserService();
                break;
            case SERVICE_MESSAGE:
                service = new MessageService();
                break;
            case SERVICE_LOGIN:
                service = new LoginService();
                break;
            case SERVICE_REGISTRATION:
                service = new RegistrationService();
                break;
            case  SERVICE_LOGOUT:
                service = new LogoutService();
                break;
            default:
                service = new AuthenticationService();
        }

        return service;
    }

    private static String getNameService(String uri) {
        getServiceMap();
        String service = EMPTY;
        for (Map.Entry entry : PATTERN_SERVICE_MAP.entrySet()) {
            Pattern pattern = Pattern.compile((String) entry.getKey());
            Matcher matcher = pattern.matcher(uri);
            if (matcher.find()) {
                service = (String) entry.getValue();
                break;
            }
        }
        return service;
    }

    private static void getServiceMap() {
        PATTERN_SERVICE_MAP.put(PATTERN_SERVICE_USER, SERVICE_USER);
        PATTERN_SERVICE_MAP.put(PATTERN_SERVICE_MESSAGE, SERVICE_MESSAGE);
        PATTERN_SERVICE_MAP.put(PATTERN_SERVICE_LOGIN, SERVICE_LOGIN);
        PATTERN_SERVICE_MAP.put(PATTERN_SERVICE_REGISTRATION, SERVICE_REGISTRATION);
        PATTERN_SERVICE_MAP.put(PATTERN_SERVICE_AUTHENTICATION, SERVICE_AUTHENTICATION);
        PATTERN_SERVICE_MAP.put(PATTERN_SERVICE_LOGOUT, SERVICE_LOGOUT);
    }

}
