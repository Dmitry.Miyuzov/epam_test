package utils;

import java.util.ResourceBundle;

public final class RegexProvider {

    private static final ResourceBundle SERVICE_EXPRESSIONS = ResourceBundle.getBundle("service_expressions");
    private static final ResourceBundle DATA_USER_EXPRESSION = ResourceBundle.getBundle("data_user_expressions");
    private static final ResourceBundle DATA_MESSAGE_EXPRESSIONS = ResourceBundle.getBundle("data_message_expressions");

    private RegexProvider(){}

    static String getServiceExpression(String key) {
        return SERVICE_EXPRESSIONS.getString(key);
    }

    static String getDataUserExpression(String key) {
        return DATA_USER_EXPRESSION.getString(key);
    }

    static String getDataMessageExpressions(String key) {
        return DATA_MESSAGE_EXPRESSIONS.getString(key);
    }
}
