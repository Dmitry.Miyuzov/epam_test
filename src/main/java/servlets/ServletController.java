package servlets;

import DAO.DAOFactory;
import DAO.DBType;
import org.apache.log4j.Logger;

import serviсes.Service;

import utils.AllocatorRequests;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletController extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(ServletController.class);
    private static final String MESSAGE_DAO = "messageDao";
    private static final String USER_DAO = "userDao";
    private static final String TOKEN_DAO = "tokenDao";
    private static final String METHOD_PATCH = "PATCH";


    @Override
    public void init() {
        DAOFactory daoFactory = DAOFactory.getInstance(DBType.POSTGRESQL);
        ServletContext context = getServletContext();
        context.setAttribute(MESSAGE_DAO, daoFactory.getMessageDAO());
        context.setAttribute(USER_DAO, daoFactory.getUserDAO());
        context.setAttribute(TOKEN_DAO, daoFactory.getTokenDAO());
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res){
        if (req.getMethod().equalsIgnoreCase(METHOD_PATCH)){
            doPatch(req, res);
        } else {
            try {
                super.service(req, res);
            } catch (ServletException | IOException e) {
                LOGGER.error(e);
            }
        }
    }

    private void doPatch(HttpServletRequest request, HttpServletResponse response) {
        Service service = AllocatorRequests.getService(request.getRequestURI());
        try {
            service.execute(request,response);
        } catch (IOException | ServletException e) {
            LOGGER.error(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  {
        Service service = AllocatorRequests.getService(request.getRequestURI());
        try {
            service.execute(request,response);
        } catch (IOException | ServletException e) {
            LOGGER.error(e);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)  {
        Service service = AllocatorRequests.getService(request.getRequestURI());
        try {
            service.execute(request,response);
        } catch (IOException | ServletException e) {
            LOGGER.error(e);
        }
    }




}
