package serviсes;

import DAO.UserDAO;
import DTO.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import utils.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class UserService implements Service {
    private static final String METHOD_PATCH = "PATCH";
    private static final String METHOD_GET = "GET";
    private static final String PART_URI_USERS = "/users/";
    private static final String PARAMETER_OLD_PASS = "oldPassword";
    private static final String PARAMETER_NEW_PASS = "newPassword";
    private static final String PARAMETER_COR_NEW_PASS = "newCorPassword";
    private static final String PROPERTY_ID_USER = "idUser";
    private static final String PROPERTY_NAME = "name";
    private static final String PROPERTY_EMAIL = "email";
    private static final String PROPERTY_TITLE_ROLE = "titleRole";
    private static final String PROPERTY_TITLE_GENDER = "titleGender";
    private static final String PROPERTY_BASE64_IMG = "base64Img";
    private static final int STATUS_200 = 200;
    private static final int STATUS_422 = 422;
    private static final String EMPTY = "";
    private String oldPassword;
    private String newPassword;
    private String newCorPassword;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        String method = req.getMethod();
        if (method.equals(METHOD_PATCH)) {
            updateUser(req, resp);
        } else if (method.equals(METHOD_GET)) {
            giveAllOnlineUser(req, resp);
        }
    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response) {
        int idUser = Integer.parseInt(request.getRequestURI().replace(PART_URI_USERS, EMPTY));
        UserDAO userDAO = ServletUtils.getUserDao(request);
        User user = userDAO.getUser(idUser);
        String password = user.getPassword();
        JsonObject jsonRequest = ReaderRequests.getRequestData(request);
        setDataFields(jsonRequest);
        if (EncoderPassword.checkPassword(oldPassword, password)
                && UserDataValidator.validateCorPassword(newPassword, newCorPassword)
                && UserDataValidator.validatePassword(newPassword)) {
            userDAO.updatePassword(idUser, EncoderPassword.hashPassword(newPassword));
            response.setStatus(STATUS_200);
        } else {
            response.setStatus(STATUS_422);
        }
    }

    private void giveAllOnlineUser(HttpServletRequest request, HttpServletResponse response) {
        UserDAO userDAO = ServletUtils.getUserDao(request);
        List<User> list = userDAO.getAllOnlineUsers();
        List<String> stringUsers = new ArrayList<>();
        for (User elem : list) {
            stringUsers.add(getStringUser(elem));
        }
        String gson = new Gson().toJson(stringUsers);
        WriterResponse.sendResponse(response, gson);
    }

    private String getStringUser(User user) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(PROPERTY_ID_USER, user.getIdUser());
        jsonObject.addProperty(PROPERTY_NAME, user.getName());
        jsonObject.addProperty(PROPERTY_EMAIL, user.getEmail());
        jsonObject.addProperty(PROPERTY_TITLE_ROLE, user.getRole().toString());
        jsonObject.addProperty(PROPERTY_TITLE_GENDER, user.getGender().toString());
        jsonObject.addProperty(PROPERTY_BASE64_IMG, user.getBase64Img());

        return jsonObject.toString();
    }


    private String getOldPassword(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_OLD_PASS).getAsString();
    }

    private String getNewPassword(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_NEW_PASS).getAsString();
    }

    private String getNewCorPassword(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_COR_NEW_PASS).getAsString();
    }

    private void setDataFields(JsonObject jsonObject) {
        oldPassword = getOldPassword(jsonObject);
        newPassword = getNewPassword(jsonObject);
        newCorPassword = getNewCorPassword(jsonObject);
    }
}
