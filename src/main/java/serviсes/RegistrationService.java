package serviсes;


import DAO.TokenDAO;
import DAO.UserDAO;
import DTO.Role;
import DTO.Gender;
import DTO.Token;
import DTO.User;
import com.google.gson.JsonObject;
import utils.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public final class RegistrationService implements Service {
    private static final int STATUS_200 = 200;
    private static final int STATUS_422 = 422;
    private static final String PARAMETER_NAME = "name";
    private static final String PARAMETER_EMAIL = "email";
    private static final String PARAMETER_PASS = "password";
    private static final String PARAMETER_COR_PASS = "cor_password";
    private static final String PARAMETER_GENDER = "gender";
    private static final String PARAMETER_REPEAT_EMAIL = "repeatEmail";
    private static final String PARAMETER_IMAGE = "image";

    private String name;
    private String email;
    private String password;
    private String corPassword;
    private String gender;
    private HashMap<String, Boolean> errorMap;


    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        JsonObject jsonRequest = ReaderRequests.getRequestData(req);
        setDataOfFields(jsonRequest);
        JsonObject jsonResponse;
        errorMap = createErrorMap();
        UserDAO userDAO = ServletUtils.getUserDao(req);
        TokenDAO tokenDAO = ServletUtils.getTokenDao(req);
        if (isDataValidate(name, email, password, corPassword)
                && !userDAO.getEmail(email).equals(email)) {
            User user = new User(name, email, EncoderPassword.hashPassword(password),
                    Gender.getGender(gender), Role.getRoleByEmail(email), ImageUtils.getBase64ImgForDatabase(getBase64Image(jsonRequest), getGender(jsonRequest)));
            userDAO.addUserToDataBase(user);
            Token token = new Token(userDAO.getIdUser(email));
            userDAO.addTokenToDataBase(token);
            tokenDAO.updateAccessTokenToDataBase(tokenDAO.createAccessToken(req, user));
            tokenDAO.updateRefreshTokenToDataBase(tokenDAO.createRefreshToken(req, user));
            resp.setStatus(STATUS_200);

        } else {
            getDataNoValidate(name, email, password, corPassword, errorMap);
            resp.setStatus(STATUS_422);
            jsonResponse = createResponseToClient();
            WriterResponse.sendResponse(resp, jsonResponse);
        }
    }


    private String getName(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_NAME).getAsString();
    }

    private String getEmail(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_EMAIL).getAsString();
    }

    private String getPassword(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_PASS).getAsString();
    }

    private String getCorPassword(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_COR_PASS).getAsString();
    }

    private String getGender(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_GENDER).getAsString();
    }

    private String getBase64Image(JsonObject jsonObject) {
        return jsonObject.get(PARAMETER_IMAGE).getAsString();
    }

    private void setDataOfFields(JsonObject jsonObject) {
        name = getName(jsonObject).trim();
        email = getEmail(jsonObject).trim().toLowerCase();
        password = getPassword(jsonObject).trim();
        corPassword = getCorPassword(jsonObject).trim();
        gender = getGender(jsonObject);
    }

    private boolean isDataValidate(String name, String email, String password, String corPassword) {
        boolean isNameValidate = UserDataValidator.validateName(name);
        boolean isEmailValidate = UserDataValidator.validateEmail(email);
        boolean isPasswordValidate = UserDataValidator.validatePassword(password);
        boolean isCorPasswordValidate = UserDataValidator.validateCorPassword(password, corPassword);

        return isNameValidate && isEmailValidate && isPasswordValidate && isCorPasswordValidate;
    }

    private void getDataNoValidate(String name, String email, String password,
                                   String corPassword, HashMap<String, Boolean> errorMap) {

        if (!UserDataValidator.validateName(name)) {
            errorMap.put(PARAMETER_NAME, true);
        }
        if (!UserDataValidator.validateEmail(email)) {
            errorMap.put(PARAMETER_EMAIL, true);
        }
        if (!UserDataValidator.validatePassword(password)) {
            errorMap.put(PARAMETER_PASS, true);
        }
        if (!UserDataValidator.validateCorPassword(password, corPassword)) {
            errorMap.put(PARAMETER_COR_PASS, true);
        }
        errorMap.put(PARAMETER_REPEAT_EMAIL, true);
    }

    private JsonObject createResponseToClient() {
        JsonObject jsonObject = new JsonObject();
        for (Map.Entry elem : errorMap.entrySet()) {
            String key = (String) elem.getKey();
            Boolean value = (Boolean) elem.getValue();
            jsonObject.addProperty(key, value);
        }
        return jsonObject;
    }

    private HashMap<String, Boolean> createErrorMap() {
        errorMap = new HashMap<>();
        errorMap.put(PARAMETER_NAME, false);
        errorMap.put(PARAMETER_EMAIL, false);
        errorMap.put(PARAMETER_PASS, false);
        errorMap.put(PARAMETER_COR_PASS, false);
        errorMap.put(PARAMETER_REPEAT_EMAIL, false);
        return errorMap;
    }
}
