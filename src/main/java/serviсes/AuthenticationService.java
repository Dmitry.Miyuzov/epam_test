package serviсes;

import DAO.TokenDAO;
import DAO.UserDAO;
import DB.PostgresTokenDAO;
import DTO.User;
import io.jsonwebtoken.Claims;
import utils.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthenticationService implements Service {
    private static final String HEADER_REFRESH_TOKEN = "refreshToken";
    private static final String HEADER_ACCESS_TOKEN = "accessToken";
    private static final String NULL = "null";
    private static final int STATUS_401 = 401;
    private static final int STATUS_200 = 200;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        String refreshToken = req.getHeader(HEADER_REFRESH_TOKEN);
        String accessToken = req.getHeader(HEADER_ACCESS_TOKEN);
        UserDAO userDAO = ServletUtils.getUserDao(req);
        TokenDAO tokenDAO = ServletUtils.getTokenDao(req);
        Claims claims;


        if (isRefreshTokenNull(refreshToken) || isAccessTokenNull(accessToken)) {
            resp.setStatus(STATUS_401);
        } else if (tokenDAO.getClaimsFromToken(accessToken) == null) {
            resp.setStatus(STATUS_401);
        } else {
            claims = tokenDAO.getClaimsFromToken(accessToken);
            int idUser = (int) claims.get(PostgresTokenDAO.ID_USER_KEY);
            User user = userDAO.getUser(idUser);
            String accessTokenInDataBase = tokenDAO.getTokenOfUser(req, user, PostgresTokenDAO.ACCESS_TOKEN);
            String refreshTokenInDataBase = tokenDAO.getTokenOfUser(req, user, PostgresTokenDAO.REFRESH_TOKEN);

            if (accessToken.equals(accessTokenInDataBase) && refreshToken.equals(refreshTokenInDataBase)) {
                long diedTimeAccessToken = tokenDAO.getDiedTimeToken(req, user, PostgresTokenDAO.ACCESS_TOKEN);
                long diedTimeRefreshToken = tokenDAO.getDiedTimeToken(req, user, PostgresTokenDAO.REFRESH_TOKEN);
                boolean isAliveAccessToken = tokenDAO.isAliveToken(diedTimeAccessToken);
                boolean isAliveRefreshToken = tokenDAO.isAliveToken(diedTimeRefreshToken);
                if (isAliveAccessToken && isAliveRefreshToken) {
                    resp.setHeader(HEADER_ACCESS_TOKEN, accessToken);
                    resp.setHeader(HEADER_REFRESH_TOKEN, refreshToken);
                    resp.setStatus(STATUS_200);
                } else if (isAliveRefreshToken) {
                    String newAccessToken = tokenDAO.createAccessToken(req, user);
                    String newRefreshToken = tokenDAO.createRefreshToken(req, user);
                    tokenDAO.updateAccessTokenToDataBase(newAccessToken);
                    tokenDAO.updateRefreshTokenToDataBase(newRefreshToken);
                    resp.setHeader(HEADER_ACCESS_TOKEN, newAccessToken);
                    resp.setHeader(HEADER_REFRESH_TOKEN, newRefreshToken);
                    resp.setStatus(STATUS_200);
                } else {
                    resp.setStatus(STATUS_401);
                }
            } else {
                resp.setStatus(STATUS_401);
            }
        }

    }

    /*
    This method checks header with key "accessToken"
    If accessToken equals null: return true
    If accessToken doesn't equals null: return false
     */
    private boolean isAccessTokenNull(String accessToken) {
        boolean isAccessTokenNull = false;
        if (accessToken.equals(NULL)) {
            isAccessTokenNull = true;
        }
        return isAccessTokenNull;
    }

    /*
    This method checks header with key "refreshToken"
    If refreshToke equals null: return true
    If refreshToken doesn't equals null: return false
     */
    private boolean isRefreshTokenNull(String refreshToken) {
        boolean isRefreshTokenNull = false;
        if (refreshToken.equals(NULL)) {
            isRefreshTokenNull = true;
        }
        return isRefreshTokenNull;
    }
}


