package serviсes;

import DAO.TokenDAO;
import DAO.UserDAO;
import DTO.User;
import com.google.gson.JsonObject;
import utils.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class LoginService implements Service {
    private static final String HEADER_REFRESH_TOKEN = "refreshToken";
    private static final String HEADER_ACCESS_TOKEN = "accessToken";
    private static final String GET_PARAMETER_EMAIL = "email";
    private static final String GET_PARAMETER_PASS = "password";
    private static final String EMPTY = "";
    private static final int STATUS_200 = 200;
    private static final int STATUS_422 = 422;
    private String email;
    private String password;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        JsonObject jsonObject = ReaderRequests.getRequestData(req);
        JsonObject jsonResponse;
        setDataOfFields(jsonObject);
        UserDAO userDAO = ServletUtils.getUserDao(req);
        boolean isHaveEmailInDataBase = !userDAO.getEmail(email).equals(EMPTY);

        if (isHaveEmailInDataBase) {
            boolean isPasswordValidate = EncoderPassword.checkPassword(password, userDAO.getPassword(email));
            if (isPasswordValidate) {
                jsonResponse = createResponseToClient(req);
                WriterResponse.sendResponse(resp, jsonResponse);
                resp.setStatus(STATUS_200);
            } else {
                resp.setStatus(STATUS_422);
            }
        } else {
            resp.setStatus(STATUS_422);
        }


    }

    private JsonObject createResponseToClient(HttpServletRequest request) {
        UserDAO userDAO = ServletUtils.getUserDao(request);
        TokenDAO tokenDAO = ServletUtils.getTokenDao(request);
        User user = userDAO.getUser(userDAO.getIdUser(email));
        JsonObject jsonObject = new JsonObject();
        String accessToken = tokenDAO.createAccessToken(request, user);
        String refreshToken = tokenDAO.createRefreshToken(request, user);
        tokenDAO.updateAccessTokenToDataBase(accessToken);
        tokenDAO.updateRefreshTokenToDataBase(refreshToken);
        jsonObject.addProperty(HEADER_ACCESS_TOKEN, accessToken);
        jsonObject.addProperty(HEADER_REFRESH_TOKEN, refreshToken);
        return jsonObject;
    }

    private String getEmail(JsonObject jsonObject) {
        return jsonObject.get(GET_PARAMETER_EMAIL).getAsString();
    }

    private String getPassword(JsonObject jsonObject) {
        return jsonObject.get(GET_PARAMETER_PASS).getAsString();
    }

    private void setDataOfFields(JsonObject jsonObject) {
        email = getEmail(jsonObject).trim().toLowerCase();
        password = getPassword(jsonObject).trim();
    }
}
