package DTO;

import java.util.Objects;

public class Token {
    private int userId;

    public Token(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return userId == token.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }
}
