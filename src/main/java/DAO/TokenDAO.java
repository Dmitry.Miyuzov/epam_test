package DAO;

import DTO.User;
import io.jsonwebtoken.Claims;

import javax.servlet.http.HttpServletRequest;

public interface TokenDAO {

    String createAccessToken(HttpServletRequest request, User user);

    String createRefreshToken(HttpServletRequest request, User user);

    void updateAccessTokenToDataBase(String accessToken);

    void updateRefreshTokenToDataBase(String refreshToken);

    String getTokenOfUser(HttpServletRequest request, User user, String nameToken);

    long getDiedTimeToken(HttpServletRequest request, User user, String nameToken);

    boolean isAliveToken(long diedTime);

    Claims getClaimsFromToken(String token);

    void resetToken(String token);
}
