package DAO;

import DTO.Message;

import java.util.List;

public interface MessageDAO {

    void sendMessage(Message message);

    List<Message> getLastMessages(int count);

    List<Message> getLastMessages(int count, int withIdMessage);

}
