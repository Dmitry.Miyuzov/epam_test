/*
1. This method accepts the token that was sent in the request - accessTokenRequest
2. This method accepts the response that waы sent by the server - response
3. If token didn't change - return false;
4. If token changed - return true;
 */
let isChangeAccessToken = function(accessTokenRequest,response) {
    let isChange = false;
    let accessTokenHeader = response.getResponseHeader(accessToken);
    if (accessTokenRequest !== accessTokenHeader) {
        isChange = true;
    }
    return isChange;
};
/*
This function takes two functions.
1. successFunction -  the function is called when the authentication status = 200
2. invalidAuthentication - the function is called when the authentication status = 401.
 */
let authenticationWithTwoStatus = function (successFunction, invalidAuthentication) {
    $.ajax({
        url: "/authentication",
        headers: {
            accessToken : localStorage.getItem(accessToken),
            refreshToken : localStorage.getItem(refreshToken)
        },
        method: "GET",
        cache: false,
        contentType: "application/json ; charset=utf-8",

        statusCode: {
            200: function (data,textStatus, response) {
                let isAccessChanged = isChangeAccessToken(localStorage.getItem(accessToken), response);
                if (isAccessChanged) {
                    let newAccessToken = response.getResponseHeader(accessToken);
                    let newRefreshToken = response.getResponseHeader(refreshToken);
                    localStorage.setItem(accessToken, newAccessToken);
                    localStorage.setItem(refreshToken, newRefreshToken);

                }
                let token = response.getResponseHeader(accessToken);
                successFunction(token);
            },
            401: function () {
                invalidAuthentication();
            }
        }
    });
};

$(document).ready(function () {
    authenticationWithTwoStatus(viewAfterLogin,showLogout);
});
