let logoutUser = function() {
    Swal.fire({
        icon: "question",
        showCancelButton: true,
        title: 'Вы действительно хотите выйти?',
        confirmButtonText: "Да",
        cancelButtonText: "Нет",
        confirmButtonColor: "#6666ff",
        cancelButtonColor: "#999999",
        allowEscapeKey: false,
        position: 'top',
    }).then(function(result) {
        if (result.value === true) {
            authenticationWithTwoStatus(logout, viewRegistrationWhenSessionInvalidated);
        }
    });
};
$(document).ready(function () {
    $(document.body).on(eventClick, classButtonLogoutUser, function () {
        authenticationWithTwoStatus(logoutUser, viewRegistrationWhenSessionInvalidated);
    });
    eventHome(classButtonHomeUser);
});