const chatAdmin = $(
        "<div class=\"chat\">" +
            "<div class=\"navbar-admin\">" +
                "<div class=\"home-admin\">" +
                    "<input type=\"button\" class=\"button-home-admin\" value=\"Главная страница\">" +
                "</div>" +
                "<div class=\"logout-admin\">" +
                    "<input type=\"button\" class=\"button-logout-admin\" value=\"Выход\">" +
                "</div>" +
                "<div class=\"setting-admin\">" +
                    "<input type=\"button\" class=\"button-setting-admin\" value=\"Настройки\">" +
                "<div class=\"setting-admin-content\">" +
                        "<a id=\"change-password\">Сменить пароль </a>" +
                    "</div>" +
                "</div>" +
            "</div>" +
        "</div>"
    );

    const chatGeneral = $(
        "<div class=\"main-container\">" +
            "<div class=\"chat-general\">" +
                "<div class=\"chat-window\">" +
                    "<div class=\"chat-list\">" +
                        "<ul id=\"myList\">" +
                            "<div class=\"load\" id=\"load\">" +
                                "<input type=\"button\" id=\"button-load-messages\" class=\"loadMessages\" value=\"Ещё\">" +
                            "</div>" +
                            "<div class=\"li-message\">" +
                            "</div>" +
                        "</ul>" +
                    "</div>" +
                    "<div class=\"chat-input\">" +
                        "<form id=\"form-message\">" +
                            "<input type=\"text\" class=\"chat-input-message\" placeholder=\"Введите сообщение\">" +
                            "<input type=\"button\" class=\"chat-input-button\" value=\"Отправить\">" +
                        "</form>" +
                    "</div>" +
                "</div>" +
                "<div class=\"chat-online-user\">" +
                    "<div class=\"chat-user-list\">" +
                        "<ul class=\"user-list\">" +
                            "<div class=\"user-list-header\">" +
                                "Пользователи онлайн:" +
                                "<hr>" +
                            "</div>" +
                            "<div class=\"user-list-li\">" +
                            "</div>" +
                "</div>" +
        "</div>" +
    "</div>"
    );

    let chatChangePassword = $(
        "<form id=\"form-change-password\">" +
        "<div class=\"change-password\">" +
            "<div class=\"change-password-header\">" +
                "<span>Изменение пароля</span>" +
            "</div>" +
            "<div class=\"change-password-input\">" +
                "<input type=\"password\" id=\"old-password\"  placeholder=\"Введите Старый Пароль\" required>" +
                "<i class=\"fa fa-eye-slash\" id=\"old-password-icon\"></i>" +
            "</div>" +
            "<div class=\"change-password-error\">" +
            "<i class=\"old-password-error\"></i>" +
            "</div>" +
            "<div class=\"change-password-input\">" +
                "<input type=\"password\" id=\"new-password\"  placeholder=\"Введите Новый Пароль\" required>" +
                "<i class=\"fa fa-eye-slash\" id=\"new-password-icon\"></i>" +
            "</div>" +
            "<div class=\"change-password-error\">" +
                "<i class=\"new-password-error\"></i>" +
            "</div>" +
            "<div class=\"change-password-input\">" +
                "<input type=\"password\" id=\"new-cor-password\"  placeholder=\"Повторите Пароль\" required>" +
                "<i class=\"fa fa-eye-slash\" id=\"new-cor-password-icon\"></i>" +
            "</div>" +
            "<div class=\"change-password-error\">" +
            "<i class=\"new-cor-password-error\"></i>" +
            "</div>" +
            "<div class=\"change-password-input\">" +
                "<input type=\"button\" id=\"change-user-submit\" value=\"Изменить\" disabled>" +
            "</div>" +
        "</div>" +
        "</form>"
    );

    let chatUser = $(
        "<div class=\"chat\">" +
            "<div class=\"navbar-user\">" +
                "<div class=\"home-user\">" +
                    "<input type=\"button\" class=\"button-home-user\" value=\"Главная страница\">" +
                "</div>" +
                "<div class=\"logout-user\">" +
                    "<input type=\"button\" class=\"button-logout-user\" value=\"Выход\">" +
                "</div>" +
            "</div>" +
        "</div>"
    );

    /*
    View button of registration and button of login
    */
    let menuLoginRegistration = $(
        "<div class=menu-login-registration>" +
            "<div class=menu-button-login-registration>" +
                "<button class=menu-button-login>Логин</button>" +
                "<button class=menu-button-registration>Регистрация</button>" +
            "</div>" +
        "</div>"
    );

    /*
    View Registration
     */
    let formRegistration = $(
        "<form id=\"form-registration\" enctype=\"multipart/form-data\">" +
            "<div class=\"menu-registration\">" +
                "<div class=\"menu-registration-info\">" +
                    "<i class=\"registration-info\"></i>" +
                 "</div>" +
                 "<div class=\"menu-registration-input\">" +
                    "<input type=\"text\" name=\"registration-name\" id=\"registration-name\" placeholder=\"Введите Имя\" required>" +
                "</div>" +
                 "<div class=menu-registration-error>" +
                    "<i class=registration-error-name ></i>" +
                "</div>" +
                "<div class=menu-registration-input>" +
                    "<input type=email name=registration-email id=registration-email placeholder=\"Введите Email\" required>" +
                "</div>" +
                "<div class=\"menu-registration-error\">" +
                    "<i class=\"registration-error-email\" ></i>" +
                "</div>" +
                "<div class=\"menu-registration-input\">" +
                    "<input type=\"password\" name=\"registration-password\" id=\"registration-password\"  placeholder=\"Введите Пароль\" required>" +
                    "<i class=\"fa fa-eye-slash\" id=\"registration-icon-password\"></i>" +
                "</div>" +
                "<div class=\"menu-registration-error\">" +
                    "<i class=\"registration-error-password\"></i>" +
                "</div>" +
                 "<div class=\"menu-registration-input\">" +
                    "<input type=\"password\" name=\"registration-cor-password\" id=\"registration-cor-password\" placeholder=\"Повторите Пароль\" required>" +
                    "<i class=\"fa fa-eye-slash\" id=\"registration-icon-cor-password\"></i>"  +
                "</div>" +
                "<div class=\"menu-registration-error\">" +
                    "<i class=\"registration-error-cor-password\" ></i>" +
                "</div>" +
                "<div class=\"menu-registration-input\" id=\"gender\">" +
                    "Укажите ваш пол:" +
                    "<select name=\"gender\" id=\"registration-gender\">" +
                        "<option value=\"male\">Мужской</option>" +
                        "<option value=\"female\">Женский</option>" +
                    "</select>" +
                "</div>" +
                "<div class=\"menu-registration-input-img\" id=\"img\">" +
                    "Загрузите фото в профиль (необязательно):" +
                    "<input type=\"file\" id=\"file\" name=\"file\" accept=\"image/*\">" +
                    "<br>" +
                    "<img id=\"registration-image\" src=\"#\"/>" +
                    "<script src=\"resources/js/image_load.js\"></script>" +
                "</div>" +
                "<div class=\"menu-registration-input\">" +
                    "<input type=\"button\" id=\"registration-submit\" value=\"Регистрация\" disabled>" +
                "</div>" +
            "</div>" +
        "</form>"
    );

    /*
    View Login
     */
    let formLogin = $(
        "<form id=\"form-login\">" +
            "<div class=\"menu-login\">" +
                "<div class=\"menu-login-input\">" +
                    "<input type=email name=login-email id=login-email placeholder=\"Введите Email\" required>" +
                "</div>" +
                "<div class=menu-login-error>" +
                    "<i class=login-error-email ></i>" +
                "</div>" +
                "<div class=\"menu-login-input\">" +
                    "<input type=\"password\" name=\"login-password\" id=\"login-password\"  placeholder=\"Введите Пароль\" required>" +
                    "<i class=\"fa fa-eye-slash\" id=\"login-icon-password\"></i>" +
                "</div>" +
                "<div class=\"menu-login-error\">" +
                    "<i class=\"login-error-password\"></i>" +
                "</div>" +
                "<div class=\"menu-login-input\">" +
                    "<input type=\"button\" id=\"login-submit\" value=\"Войти\" disabled>" +
                "</div>" +
            "</div>" +
        "</form>"

    );

