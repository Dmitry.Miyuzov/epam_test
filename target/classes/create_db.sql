CREATE DATABASE "chat_online";
/*
 1.Переключится на базу chat_online
 2.Выполнить код ниже.
 */

CREATE TABLE "Gender"
(
    id_gender serial UNIQUE PRIMARY KEY ,
    title_gender varchar(10) NOT NULL

);
CREATE table "Role"
(
    id_role serial UNIQUE PRIMARY KEY ,
    title_role VARCHAR(30) NOT NULL,
    description VARCHAR(30) NOT NULL
);
CREATE table "Status"
(
  id_status serial UNIQUE PRIMARY KEY ,
  title_status varchar(30) NOT NULL ,
  description varchar(30) NOT NULL
);
CREATE table "User"
(
    id_user  serial UNIQUE PRIMARY KEY ,
    id_role  int  NOT NULL ,
    id_gender   int NOT NULL ,
    name     varchar(20)        NOT NULL,
    email    varchar(50) UNIQUE NOT NULL,
    password text       NOT NULL,
    image_base64 text NOT NULL,
    FOREIGN KEY (id_role) REFERENCES "Role" (id_role),
    FOREIGN KEY (id_gender) REFERENCES "Gender" (id_gender)
);
CREATE table "Token"
(
    id_user             int UNIQUE ,
    access_token        text,
    access_create_time  timestamp(0),
    access_life_time    int,
    access_died_time    timestamp(0),
    refresh_token       text,
    refresh_create_time timestamp(0),
    refresh_life_time   int,
    refresh_died_time   timestamp(0),
    FOREIGN KEY (id_user) REFERENCES "User" (id_user)
);
CREATE TABLE "Message" (
     id_message         serial UNIQUE PRIMARY KEY ,
     user_email         varchar(50) ,
     message            VARCHAR(250) NOT NULL,
     timestamp          TIMESTAMP(3),
     status             int NOT NULL,
    FOREIGN KEY (user_email) REFERENCES "User" (email),
    FOREIGN KEY (status) REFERENCES "Status" (id_status)
                       );

INSERT INTO "Role"
VALUES (DEFAULT, 'ADMIN', 'This is role - ADMIN');
INSERT INTO "Role"
VALUES (DEFAULT, 'USER', 'This is role - USER');
INSERT INTO "Gender"
VALUES (DEFAULT, 'MALE');
INSERT INTO "Gender"
VALUES (DEFAULT, 'FEMALE');
INSERT INTO "Status"
VALUES (DEFAULT, 'MESSAGE', 'Юзер оставил сообщение');



