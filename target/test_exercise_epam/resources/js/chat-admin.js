let changePropertiesOfChangeButton = function() {
    if (isReadyNewPassword && isReadyNewCorPassword) {
        $(idChangeUserSubmit).removeAttr(textDisabled);
    } else {
        $(idChangeUserSubmit).attr(textDisabled, textDisabled);
    }
};

let emptyOldPassword = function(){
    $(idOldPassword).css(textBorder, textEmpty);
    $(idOldPassword).val(textEmpty);
    $(classOldPasswordError).html(textEmpty);
    changePropertiesOfChangeButton();
};
let emptyNewPassword = function(){
    $(idNewPassword).css(textBorder, textEmpty);
    $(idNewPassword).val(textEmpty);
    $(classNewPasswordError).html(textEmpty);
    isReadyNewPassword = false;
    changePropertiesOfChangeButton();
};
let trueNewPassword = function() {
    $(idNewPassword).css(textBorder, "2px solid #336600");
    $(classNewPasswordError).css(textColor, colorGreen);
    $(classNewPasswordError).html("Пароль корректен");
    isReadyNewPassword = true;
    changePropertiesOfChangeButton();
};
let errorIncorrectNewPassword = function() {
    $(idNewPassword).css(textBorder, "2px solid #e34761");
    $(classNewPasswordError).css(textColor,  colorRed);
    $(classNewPasswordError).html("Пароль должен содержать латинские буквы и цифры. От 5 до 16 символов.");
    isReadyNewPassword = false;
    changePropertiesOfChangeButton();
};
let emptyNewCorPassword = function() {
    $(idNewCorPassword).css(textBorder, textEmpty);
    $(idNewCorPassword).val(textEmpty);
    $(classNewCorPasswordError).html(textEmpty);
    isReadyNewCorPassword = false;
    changePropertiesOfChangeButton();
};
let trueNewCorPassword = function() {
    $(idNewCorPassword).css(textBorder, "2px solid #336600");
    $(classNewCorPasswordError).css(textColor, colorGreen);
    $(classNewCorPasswordError).html("Пароли совпадают");
    isReadyNewCorPassword = true;
    changePropertiesOfChangeButton();
};
let errorIncorrectNewCorPassword = function() {
    $(classNewCorPasswordError).html("Пароль не совпадает!");
    $(idNewCorPassword).css(textBorder, "2px solid #e34761");
    $(classNewCorPasswordError).css(textColor, colorRed);
    $(idChangeUserSubmit).attr(textDisabled, textDisabled);
    isReadyNewCorPassword = false;
    changePropertiesOfChangeButton();
};
let validateNewCorPassword = function(passwordValue, corPasswordValue) {
    if (passwordValue !== corPasswordValue) {
        errorIncorrectNewCorPassword();
    } else {
        trueNewCorPassword();
    }
};

let logoutAdmin = function() {
    Swal.fire({
        icon: "question",
        showCancelButton: true,
        title: 'Вы действительно хотите выйти?',
        confirmButtonText: "Да",
        cancelButtonText: "Нет",
        confirmButtonColor: "#6666ff",
        cancelButtonColor: "#999999",
        allowEscapeKey: false,
        heightAuto: false,
        position: 'top',
    }).then(function(result) {
        if (result.value === true) {
           authenticationWithTwoStatus(logout, viewRegistrationWhenSessionInvalidated);
        }
    });
};

let showChangePasswordMenu = function() {
    $(divMainContainer).remove();
    $(chatChangePassword).appendTo(textBody);
};
let sendChangesAboutUser = function() {
    let accessTokenString = localStorage.getItem(accessToken);
    let idUser = getIdUser(accessTokenString);
    let formChangesPassword = JSON.stringify({
        'oldPassword' : $(idOldPassword).val(),
        'newPassword' : $(idNewPassword).val(),
        'newCorPassword' : $(idNewCorPassword).val(),
    });

    $.ajax({
        url: "/users/" + idUser,
        method: "Patch",
        cache: false,
        contentType: "application/json ; charset=utf-8",
        data: formChangesPassword,
        dataType: "json",

        statusCode: {
            200: function () {
                emptyOldPassword();
                emptyNewPassword();
                emptyNewCorPassword();
                Swal.fire({
                    icon: "success",
                    title: 'Пароль успешно изменен!',
                    confirmButtonText: "Ок",
                    confirmButtonColor: "#6666ff",
                    allowEscapeKey: false,
                    position: 'top',
                });
            },
            422: function () {
                emptyOldPassword();
                emptyNewPassword();
                emptyNewCorPassword();
                Swal.fire({
                    icon: "error",
                    title: 'Данные не верны.',
                    confirmButtonText: "Ок",
                    confirmButtonColor: "#6666ff",
                    allowEscapeKey: false,
                    position: 'top',
                });
            }
        }
    });
};


$(document).ready(function () {
    eventHome(classButtonHomeAdmin);

    $(document.body).on(eventClick, classButtonLogoutAdmin, function () {
        authenticationWithTwoStatus(logoutAdmin, viewRegistrationWhenSessionInvalidated);
    });

    $(document.body).on(eventClick, idChangePassword, function () {
        authenticationWithTwoStatus(showChangePasswordMenu,viewRegistrationWhenSessionInvalidated);
    });
    $(document.body).on(eventClick, idChangeUserSubmit, function () {
       authenticationWithTwoStatus(sendChangesAboutUser,viewRegistrationWhenSessionInvalidated)
    });

    $(document.body).on(eventKeyUp,idNewPassword, function () {
        let pattern = new RegExp(patternForPassword);
        let passwordValue = $(this).val();
        let corPasswordValue = $(idNewCorPassword).val();


        if (passwordValue !== textEmpty) {
            if (pattern.test(passwordValue)) {
                trueNewPassword();
            } else {
                errorIncorrectNewPassword();
            }


            if (corPasswordValue !== textEmpty) {
                validateNewCorPassword(passwordValue,corPasswordValue);
            } else {
                emptyNewCorPassword();
            }

        } else {
            emptyNewPassword();
        }
    });

    $(document.body).on(eventKeyUp,idNewCorPassword, function () {
        let passwordValue= $(idNewPassword).val();
        let corPasswordValue = $(this).val();
        if (corPasswordValue !== textEmpty && passwordValue !== textEmpty) {
            validateNewCorPassword(passwordValue,corPasswordValue);
        } else {
            emptyNewCorPassword();
        }
    });

    controlIconPassword(idOldPasswordIcon, idOldPassword);
    controlIconPassword(idNewCorPasswordIcon, idNewCorPassword);
    controlIconPassword(idNewPasswordIcon, idNewPassword);

});