
/*
Check all fields of the registration and changing the button property to available or not
*/
let changePropertiesOfRegistrationButton = function() {
    if (isReadyName && isReadyEmail && isReadyPassword && isReadyCorPassword) {
        $(idRegistrationSubmit).removeAttr(textDisabled);
    } else {
        $(idRegistrationSubmit).attr(textDisabled, textDisabled);
    }
};
/*
These functions change style of the Email field and it value, also it changes button property to available or not.
 */
let emptyEmail = function() {
    $(idRegistrationEmail).css(textBorder, textEmpty);
    $(classRegistrationErrorEmail).html(textEmpty);
    isReadyEmail = false;
    changePropertiesOfRegistrationButton();
};
let trueEmail = function() {
    $(idRegistrationEmail).css(textBorder, "2px solid #336600");
    $(classRegistrationErrorEmail).css(textColor, colorGreen);
    $(classRegistrationErrorEmail).html("Адрес почты корректный");
    isReadyEmail = true;
    changePropertiesOfRegistrationButton();
};
let errorIncorrectEmail = function() {
    $(idRegistrationEmail).css(textBorder, "2px solid #e34761");
    $(classRegistrationErrorEmail).css(textColor, colorRed);
    $(classRegistrationErrorEmail).html("Адрес почты не корректен");
    isReadyEmail = false;
    changePropertiesOfRegistrationButton();
};
let errorRepeatEmail = function() {
    $(idRegistrationEmail).css(textBorder, "2px solid #e34761");
    $(classRegistrationErrorEmail).css(textColor, colorRed);
    $(classRegistrationErrorEmail).html("Адрес почты уже занят");
    isReadyEmail = false;
    changePropertiesOfRegistrationButton();
};
/*
These functions change style of the name field and it value, also it changes button property to available or not
 */
let emptyName = function() {
    $(idRegistrationName).css(textBorder, textEmpty);
    $(classRegistrationErrorName).html(textEmpty);
    isReadyName = false;
    changePropertiesOfRegistrationButton();
};
let trueNameRegistration =function (){
    $(idRegistrationName).css(textBorder, "2px solid #336600");
    $(classRegistrationErrorName).css(textColor, colorGreen);
    $(classRegistrationErrorName).html("Имя корректно");
    isReadyName = true;
    changePropertiesOfRegistrationButton();
};
let errorIncorrectName = function() {
    $(idRegistrationName).css(textBorder, "2px solid #e34761");
    $(classRegistrationErrorName).css(textColor, colorRed);
    $(classRegistrationErrorName).html("Имя должно состоять только из букв. Не менее 3. Не более 20");
    isReadyName = false;
    changePropertiesOfRegistrationButton();
};
/*
These functions change style of the password field and it value, also it changes button property available or not.
 */
let emptyPassword = function(){
    $(idRegistrationPassword).css(textBorder, textEmpty);
    $(classRegistrationErrorPassword).html(textEmpty);
    isReadyPassword = false;
    changePropertiesOfRegistrationButton();
};
let truePassword = function() {
    $(idRegistrationPassword).css(textBorder, "2px solid #336600");
    $(classRegistrationErrorPassword).css(textColor, colorGreen);
    $(classRegistrationErrorPassword).html("Пароль корректен");
    isReadyPassword = true;
    changePropertiesOfRegistrationButton();
};
let errorIncorrectPassword = function() {
    $(idRegistrationPassword).css(textBorder, "2px solid #e34761");
    $(classRegistrationErrorPassword).css(textColor, colorRed);
    $(classRegistrationErrorPassword).html("Пароль должен содержать латинские буквы и цифры. От 5 до 16 символов.");
    isReadyPassword = false;
    changePropertiesOfRegistrationButton();
};
/*
These functions change style of the cor password field and it value, also it changes button property available or not.
 */
let emptyCorPassword = function() {
    $(idRegistrationCorPassword).css(textBorder, textEmpty);
    $(classRegistrationErrorCorPassword).html(textEmpty);
    isReadyCorPassword = false;
    changePropertiesOfRegistrationButton();
};
let trueCorPassword = function() {
    $(idRegistrationCorPassword).css(textBorder, "2px solid #336600");
    $(classRegistrationErrorCorPassword).css(textColor, colorGreen);
    $(classRegistrationErrorCorPassword).html("Пароли совпадают");
    isReadyCorPassword = true;
    changePropertiesOfRegistrationButton();
};
let errorIncorrectCorPassword = function() {
    $(classRegistrationErrorCorPassword).html("Пароль не совпадает!");
    $(idRegistrationCorPassword).css(textBorder, "2px solid #e34761");
    $(classRegistrationErrorCorPassword).css(textColor, colorRed);
    $(idRegistrationSubmit).attr(textDisabled, textDisabled);
    isReadyCorPassword = false;
    changePropertiesOfRegistrationButton();
};
/*
Parameter name: accepts type Boolean.
If name "true": The function "errorIncorrectName" will be called.
This method is needed for server validation
 */
let validateServerName = function(name) {
    if (name) {
        errorIncorrectName();
    }
};

/*
Parameter incorrectEmail, repeatEmail: accept type Boolean.
If incorrectEmail "true": The function "errorIncorrectEmail" will be called.
If repeatEmail "true": The function "errorRepeatEmail" will be called.

This method is needed for server validation
 */
let validateServerEmail = function(incorrectEmail, repeatEmail) {
    if (incorrectEmail) {
        errorIncorrectEmail();
    }
    if (repeatEmail) {
        errorRepeatEmail();
    }
};
/*
Parameter password: accepts type Boolean.
if password "true": The function "errorIncorrectPassword" will be called.

This method is needed for server validation
 */
let validateServerPassword = function(password) {
    if (password) {
        errorIncorrectPassword();
    }
};
/*
Parameter corPassword: accepts type Boolean.
if corPassword "true": The function "errorIncorrectCorPassword" will be called.

This method is needed for server validation
 */
let validateServerCorPassword = function(corPassword) {
    if (corPassword) {
        errorIncorrectCorPassword();
    }
};
/*
Parameter passwordValue, corPasswordValue: accepts type String.
This method is needed for client-side validation.
 */
let validateCorPassword = function(passwordValue, corPasswordValue) {
    if (passwordValue !== corPasswordValue) {
        errorIncorrectCorPassword();
    } else {
        trueCorPassword();
    }
};
/*
This function clears all errors' fields of registration.
 */
let clearAllError = function() {
    $(classRegistrationErrorName).empty();
    $(classRegistrationErrorEmail).empty();
    $(classRegistrationErrorCorPassword).empty();
    $(classRegistrationErrorPassword).empty();
};

/*
This function clears all fields of registration.
 */
let clearFieldsRegistration = function() {
    $(idRegistrationName).val(textEmpty);
    $(idRegistrationEmail).val(textEmpty);
    $(idRegistrationCorPassword).val(textEmpty);
    $(idRegistrationPassword).val(textEmpty);
    $(idFile).val(textEmpty);
    $(idRegistrationImage).attr(textSrc,"#");
};
/*
This function changes style all button on default
 */
let comeBackDefaultStyleForButton = function() {
    $(idRegistrationName).css(textBorder, textEmpty);
    $(idRegistrationEmail).css(textBorder, textEmpty);
    $(idRegistrationPassword).css(textBorder, textEmpty);
    $(idRegistrationCorPassword).css(textBorder, textEmpty);
};
/*
This function change property button of registration on disabled
 */
let disableRegistrationButton = function() {
    $(idRegistrationSubmit).attr(textDisabled, textDisabled);
};
/*
This function scrolls page on menu
 */
let scrollPageToMenu = function() {
    $('html, body').animate({
        scrollTop: $(classMenuLoginRegistration).offset().top
    }, textSlow);
};
/*
This function will be called when registration will be successful and user come back in registration
 */
let comeBackAfterSuccessfulRegistration = function() {
    scrollPageToMenu();
    clearAllError();
    clearFieldsRegistration();
    comeBackDefaultStyleForButton();
    disableRegistrationButton();
};
/*
This function will be called when registration will be successful and user wants to go login
 */
let comeLoginAfterSuccessfulRegistration = function() {
    let email = $(idRegistrationEmail).val().toString();
    clearAllError();
    clearFieldsRegistration();
    comeBackDefaultStyleForButton();
    disableRegistrationButton();
    showLoginForm(email);
    $(idLoginEmail).css(textBorder, "2px solid #336600");
    $(classLoginErrorEmail).css(textColor, colorGreen);
    $(classLoginErrorEmail).html("Адрес почты корректный");
    isReadyLoginEmail = true;
};
/*
This function will be called when registration will fail
 */
let unsuccessfulRegistration = function(name, email, repeatEmail, password, cor_password) {
    validateServerName(name);
    validateServerEmail(email,repeatEmail);
    validateServerPassword(password);
    validateServerCorPassword(cor_password);
    scrollPageToMenu();
};


$(document).ready(function () {
    /*
    When we hover the cursor on button of registration
    */
    $(document).on({
        mouseenter: function () {
            $(classMenuButtonRegistration).css(textBackgroundColor, '#0e5699')
        },
        mouseleave: function () {
            $(classMenuButtonRegistration).css(textBackgroundColor, '#77b3d6')
        }
    }, classMenuButtonRegistration);


    /*
    Validation of the field name
    */
    $(document.body).on(eventKeyUp,idRegistrationName, function () {

        let pattern = new RegExp(patternForName);
        let nameValue = $(this).val();

        if (nameValue !== textEmpty) {
            if (pattern.test(nameValue)) {
                trueNameRegistration();
            } else {
                errorIncorrectName()
            }
        } else {
            emptyName();
        }
    });

    /*
    Validation of the field email
    */
    $(document.body).on(eventKeyUp,idRegistrationEmail, function () {

        let pattern = new RegExp(patternForEmail);
        let emailValue = $(this).val();

        if (emailValue !== textEmpty) {
            if (pattern.test(emailValue)) {
                trueEmail();
            } else {
                errorIncorrectEmail();
            }
        } else {
            emptyEmail();
        }
    });

    /*
    Validation of the field password
    */
    $(document.body).on(eventKeyUp,idRegistrationPassword, function () {
        let pattern = new RegExp(patternForPassword);
        let passwordValue = $(this).val();
        let corPasswordValue = $(idRegistrationCorPassword).val();


        if (passwordValue !== textEmpty) {
            if (pattern.test(passwordValue)) {
                truePassword();
            } else {
                errorIncorrectPassword();
            }

            //Delete it then create error
            if (corPasswordValue !== textEmpty) {
                validateCorPassword(passwordValue,corPasswordValue);
            } else {
                emptyCorPassword();
            }

        } else {
            emptyPassword();
        }
    });

    /*
    Validation Cor Password
    */
    $(document.body).on(eventKeyUp,idRegistrationCorPassword, function () {

        let passwordValue= $(idRegistrationPassword).val();
        let corPasswordValue = $(this).val();
        if (corPasswordValue !== textEmpty && passwordValue !== textEmpty) {
            validateCorPassword(passwordValue,corPasswordValue);
        } else {
            emptyCorPassword();
        }
    });

    /*
      Control hide/show password.
    */
    controlIconPassword(idRegistrationIconPassword, idRegistrationPassword);
    controlIconPassword(idRegistrationIconCorPassword, idRegistrationCorPassword);

    $(document.body).on(eventClick,idRegistrationSubmit, function() {
        let formRegistration = JSON.stringify({
            'name': $(idRegistrationName).val().trim(),
            'email': $(idRegistrationEmail).val().trim(),
            'password': $(idRegistrationPassword).val().trim(),
            'cor_password': $(idRegistrationCorPassword).val().trim(),
            'gender': $(idRegistrationGender).val(),
            'image' : $(idRegistrationImage).attr(textSrc),
        });
        $.ajax({
            url: "/registration",
            cache: false,
            method: "post",
            contentType: "application/json ; charset=utf-8",
            data: formRegistration,
            dataType: "json",

            statusCode: {
                200: function () {
                    Swal.fire({
                        icon: "success",
                        showCancelButton: true,
                        title: 'Регистрация успешно завершена',
                        text: "Хотите перейти к странице логина?",
                        confirmButtonText: "Да",
                        cancelButtonText: "Нет",
                        confirmButtonColor: "#6666ff",
                        cancelButtonColor: "#999999",
                        allowEscapeKey: false,
                        position: 'top',
                    }).then(function(result) {
                        if (result.value === true) {
                            comeLoginAfterSuccessfulRegistration();
                        }
                        if (result.dismiss === 'cancel') {
                            comeBackAfterSuccessfulRegistration();
                        }
                    });

                },
                422: function (jsonFromServer) {
                    console.log(jsonFromServer);
                    let error = JSON.parse(jsonFromServer.responseText);
                    let repeatEmail = error.repeatEmail;
                    let name = error.name;
                    let password = error.password;
                    let cor_password = error.cor_password;
                    let email = error.email;
                    unsuccessfulRegistration(name, email, repeatEmail, password, cor_password);
                }
            }
        });
    });


});
